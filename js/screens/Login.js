import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Alert,
    ActivityIndicator
} from 'react-native';
import { SocialIcon, Header, Button } from 'react-native-elements';
import { StackNavigator } from 'react-navigation';
import Colors from '@shared/Colors';
import styled from 'styled-components/native'
import t from 'tcomb-form-native';
import cloneDeep from 'lodash/fp/cloneDeep';
import Api from '@shared/Api';

const Register = styled.Text`
    color: #fff;
    text-align: center;
    margin-top: 30px;
`

const Form = t.form.Form;
const stylesheet = cloneDeep(t.form.Form.stylesheet);

stylesheet.textbox.normal.color = Colors.whiteColor;
stylesheet.textbox.normal.borderWidth = 0;
stylesheet.textbox.normal.borderBottomWidth = 1;
stylesheet.textbox.normal.borderColor = Colors.whiteColor;
stylesheet.controlLabel.normal.color = Colors.whiteColor;
stylesheet.controlLabel.normal.fontSize = 13;

//error style
stylesheet.textbox.error.color = Colors.whiteColor;
stylesheet.textbox.error.borderWidth = 0;
stylesheet.textbox.error.borderBottomWidth = 1;
stylesheet.textbox.error.borderColor = Colors.whiteColor;
stylesheet.textbox.error.color = Colors.whiteColor;
stylesheet.controlLabel.error.color = Colors.whiteColor;

const LoginForm = t.struct({
    email: t.String,
    password: t.String
});

export class Login extends Component {
    static navigationOptions = {
        title: 'Login',
        header: null
    };

    constructor(props) {
        super(props);

        this.state = {
            loginForm: {
                email: {
                    value: '',
                    hasError: false,
                    pristine: true,
                    error: null
                },
                password: {
                    value: '',
                    hasError: false,
                    pristine: true,
                    error: null
                }
            },
            isFetching: false,
            error: null
        };
    }

    updateFormState(field, value, error) {
        let { loginForm } = this.state;
        this.setState({
            loginForm: {
                ...loginForm,
                [field]: {
                    pristine: false,
                    hasError: !!error,
                    value,
                    error
                }
            },
            error: null
        });
    }

    onChangeForm = value => {
        let { loginForm } = this.state;
        if (value.email !== loginForm.email.value) {
            let error = null;
            if (value.email.length === 0) {
                error = 'Digite um email válido';
            }

            this.updateFormState('email', value.email, error);
        }

        if (value.password !== loginForm.password.value) {
            let error = null;
            if (value.password.length === 0) {
                error = 'Digite sua senha';
            }

            this.updateFormState('password', value.password, error);
        }
    };

    goBack() {
        this.props.navigation.goBack();
    }

    login = async () => {
        let { loginForm } = this.state;
        if (
            !loginForm.email.pristine &&
            !loginForm.password.pristine &&
            !loginForm.email.hasError &&
            !loginForm.password.hasError
        ) {
            this.setState({ isFetching: true });

            try {
                let userForm = {
                    email: loginForm.email.value,
                    password: loginForm.password.value
                };

                let user = await Api.login(userForm);

                this.props.screenProps.onUserUpdate &&
                    this.props.screenProps.onUserUpdate(user);

                this.goBack();
            } catch (error) {
                this.setState({
                    error
                });
            }

            this.setState({ isFetching: false });
        } else {
            Alert.alert('Atenção', 'Preencha email e senha');
        }
    };

    render() {
        let { loginForm, isFetching } = this.state;

        const options = {
            stylesheet,
            fields: {
                email: {
                    keyboardType: 'email-address',
                    editable: !isFetching,
                    hasError: loginForm.email.hasError,
                    error: loginForm.email.error,
                    value: loginForm.email.value,
                    label: 'E-mail',
                    autoCapitalize: 'none',
                    autoCorrect: false,
                    clearButtonMode: 'always',
                },
                password: {
                    secureTextEntry: true,
                    editable: !isFetching,
                    label: 'Senha',
                    hasError: loginForm.password.hasError,
                    error: loginForm.password.error,
                    value: loginForm.password.value
                }
            }
        };

        const formValue = {
            email: loginForm.email.value,
            password: loginForm.password.value
        };

        return (
            <View style={styles.container}>
                <View style={styles.imgContainer}>
                    <Image
                        resizeMode='contain'
                        source={require('../resources/logo.png')}
                        style={{ flex: 3 }}
                    />
                </View>
                <View style={styles.formContainer}>
                    <Form
                        ref='form'
                        type={LoginForm}
                        options={options}
                        value={formValue}
                        onChange={this.onChangeForm}
                    />
                    <Button backgroundColor={Colors.whiteColor} style={{ marginTop: 10, borderRadius: 4 }} color={Colors.primaryColor} title='Entrar' onPress={this.login} />
                    <Register onPress={() => this.props.navigation.navigate('RegisterUser')}>
                        CADASTRAR
                    </Register>
                    {isFetching && (
                        <ActivityIndicator
                            color={'#fff'}
                            size={'large'}
                        />
                    )}
                </View>
                {this.state.error && (
                    <Text style={styles.errorMessage}>{this.state.error.message}</Text>
                )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: Colors.primaryColor
    },
    imgContainer: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    formContainer: {
        flex: 3,
        marginHorizontal: 16,
        alignItems: 'stretch',
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    forgotPasswordText: {
        color: Colors.lighterText,
        fontSize: 12,
        alignSelf: 'center'
    },
    errorMessage: {
        alignSelf: 'center',
        fontWeight: 'bold',
        marginBottom: 8,
        color: Colors.errorColor
    },
});

export default Login;