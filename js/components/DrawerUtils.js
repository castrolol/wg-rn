import React from "react";
import { View, TouchableOpacity } from "react-native";
import { Icon } from "react-native-elements";

import Colors from "@shared/Colors";

const RenderDrawerMenu = navigation => {
    return (
        <View style={{ marginRight: 14 }}>
            <Icon
                name="menu"
                component={TouchableOpacity}
                color={Colors.primaryColor}
                size={27}
                onPress={() => navigation.navigate("DrawerToggle")}
            />
        </View>
    );
};

export { RenderDrawerMenu };