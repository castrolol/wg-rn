import React from 'react'

import { View, Text, Image, TouchableOpacity } from 'react-native'
import { SocialIcon } from 'react-native-elements'
import styled from 'styled-components/native'
import Colors from '@shared/Colors'

const logo = require('../resources/logo.png')

const Container = styled.View`
    flex: 1;
    background-color: ${Colors.primaryColor};
    justify-content: space-around;
    align-items: center;

`
const ImageLogo = styled.Image``
const Button = styled.TouchableOpacity`
    padding: 13px;
    border-radius: 30px;
    background-color: ${Colors.whiteColor};
    width: 250px;
    margin-top: 10px;
`
const TextButton = styled.Text`
    color: ${Colors.primaryColor};
    text-align: center;
    font-weight: bold;
`
const ContainerLogo = styled.View`
    flex: 1;
    justify-content: center;
`
const ContainerButton = styled.View`
    justify-content: flex-start;
    flex: 1;
`

export default class Initial extends React.Component {
    static navigationOptions = {
        header: null
    };
    render() {
        return (
            <Container>
                <ContainerLogo>
                    <ImageLogo resizeMode="contain" source={logo} />
                </ContainerLogo>
                <ContainerButton>
                    <Button onPress={() => this.props.navigation.navigate('Login')}>
                        <TextButton>Buscar Estabelecimentos</TextButton>
                    </Button>
                </ContainerButton>
            </Container>
        )
    }
}