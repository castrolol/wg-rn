import React, { Component } from "react";
import {
    View,
    ScrollView,
    TouchableOpacity,
    StyleSheet,
    Image
} from "react-native";
import { DrawerNavigator, DrawerItems } from "react-navigation";
import { Icon, List, ListItem, Text } from "react-native-elements";

import Colors from "@shared/Colors";

const CustomDrawerContentComponent = ({
  inactiveTintColor,
    itemsContainerStyle,
    ...props
}) => {
    const logout = () => {
        props.navigation.navigate("Initial");
        props.screenProps.onUserUpdate && props.screenProps.onUserUpdate(null);
    };
    const { user } = props.screenProps;
    return (
        <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0, flex: 5 }}>
            <View style={styles.drawerHeader}>
                <View style={{ justifyContent: "center", marginHorizontal: 8 }}>
                    {/* <Image
                        source={{
                            uri:
                        }}
                        style={{
                            width: 60,
                            height: 60,
                            borderRadius: 30
                        }}
                    /> */}
                </View>
                <View style={{ justifyContent: "center", marginHorizontal: 8 }}>
                    <Text h4 style={styles.drawerHeaderText}>
                        {user.username}
                    </Text>
                    <Text style={styles.drawerHeaderText}>{user.email}</Text>
                </View>
            </View>
            <ScrollView style={{ flex: 1 }}>
                <DrawerItems {...props} />
                <View style={[styles.container, itemsContainerStyle]}>
                    <TouchableOpacity onPress={logout}>
                        <View style={styles.item}>
                            <View style={styles.icon}>
                                <Icon name="exit-to-app" color={Colors.darkText} />
                            </View>
                            <Text style={[styles.label, { color: inactiveTintColor }]}>
                                Sair
              </Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </List>
    );
};

export default DrawerNavigator(
    {
        Establishments: { screen: require("@screens/establishments").default },
    },
    {
        initialRouteName: "Establishments",
        headerMode: "screen",
        drawerPosition: 'right',
        contentOptions: {
            activeTintColor: Colors.primaryColor
        },
        drawerBackgroundColor: Colors.primaryColor,
        contentComponent: CustomDrawerContentComponent
    }
);

const styles = StyleSheet.create({
    item: {
        flexDirection: "row",
        alignItems: "center"
    },
    icon: {
        marginHorizontal: 16,
        width: 24,
        alignItems: "center"
    },
    label: {
        margin: 16,
        fontWeight: "bold",
        color: Colors.darkText
    },
    drawerHeader: {
        height: 120,
        backgroundColor: Colors.primaryColor,
        justifyContent: "center",
        alignContent: "center",
        flexDirection: "row"
    },
    drawerHeaderText: {
        color: Colors.lighterText
    }
});