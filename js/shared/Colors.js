export default {
    primaryColor: "#e23841",
    whiteColor: "#f8f8f8",
    tintColor: '#ae0811'
};
