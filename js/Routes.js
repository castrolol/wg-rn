import { StackNavigator } from "react-navigation";
import Colors from "@shared/Colors";

const RouteConfigMap = {
    Home: {
        path: "/",
        screen: require("./Home").default,
        navigationOptions: {
            header: null
        }
    },
    Initial: {
        path: "/initial",
        screen: require("@screens/Initial").default,
        mode: "modal",

    },
    Login: {
        path: "/login",
        screen: require("@screens/Login").default,
        mode: "modal",
        navigationOptions: {
            gesturesEnabled: false
        }
    },
    RegisterUser: {
        path: "/registerUser",
        screen: require("@screens/RegisterUser").default,
        mode: "modal",
        navigationOptions: {
            gesturesEnabled: false
        }
    },
    RegisterPassword: {
        path: "/registerPassword",
        screen: require("@screens/RegisterPassword").default,
        mode: "modal",
        navigationOptions: {
            gesturesEnabled: false
        }
    },

    // Register: {
    //     path: "/register",
    //     screen: require("@screens/Register").default,
    //     mode: "modal",
    //     navigationOptions: {
    //         gesturesEnabled: false
    //     }
    // }
};

const StackConfig = {
    initialRouteName: "Home",
    navigationOptions: {
        headerBackTitle: null,
        headerTintColor: Colors.whiteColor,
        headerStyle: {
            backgroundColor: Colors.primaryColor
        }
    }
};

const LoggedInRoutes = StackNavigator(RouteConfigMap, StackConfig);
const LoggedOutRoutes = StackNavigator(RouteConfigMap, {
    ...StackConfig,
    initialRouteName: "Initial"
});

export { LoggedInRoutes, LoggedOutRoutes };