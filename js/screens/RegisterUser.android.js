import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Alert,
} from "react-native";
import { SocialIcon, Header } from "react-native-elements";
import { StackNavigator } from "react-navigation";
import styled from 'styled-components/native'
import t from "tcomb-form-native";
import Api from "@shared/Api";
import Colors from "@shared/Colors";
import { ActivityIndicator } from "react-native";
import stylesheet from '@shared/FormStyle'
import ContinueButton from '@components/ContinueButton'

const Register = styled.Text`
    color: ${Colors.primaryColor};
    text-align: center;
    margin-top: 30px;
`


const Form = t.form.Form;

const RegisterUserForm = t.struct({
    name: t.String,
    email: t.String,
    cpf: t.String,
    dob: t.String
});

export class RegisterUser extends Component {
    static navigationOptions = {
        title: "Faça seu Cadastro",
        headerStyle: {
            backgroundColor: Colors.primaryColor
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            registerUserForm: {
                email: {
                    value: "",
                    hasError: false,
                    pristine: true,
                    error: null
                },
                dob: {
                    value: "",
                    hasError: false,
                    pristine: true,
                    error: null
                },
                name: {
                    value: "",
                    hasError: false,
                    pristine: true,
                    error: null
                },
                cpf: {
                    value: "",
                    hasError: false,
                    pristine: true,
                    error: null
                },
            },
            isFetching: false,
            error: null
        };
    }

    updateFormState(field, value, error) {
        let { registerUserForm } = this.state;
        this.setState({
            registerUserForm: {
                ...registerUserForm,
                [field]: {
                    pristine: false,
                    hasError: !!error,
                    value,
                    error
                }
            },
            error: null
        });
    }

    onChangeForm = value => {
        let { registerUserForm } = this.state;

        if (value.email !== registerUserForm.email.value) {
            let error = null;
            if (value.email.length === 0) {
                error = "Digite um email válido";
            }
            this.updateFormState("email", value.email, error);
        }

        if (value.name !== registerUserForm.name.value) {
            let error = null;
            if (value.name.length === 0) {
                error = "Digite um nome";
            }
            this.updateFormState("name", value.name, error);
        }

        if (value.dob !== registerUserForm.dob.value) {
            let error = null;
            if (value.dob.length === 0) {
                error = "Digite uma Data de Nascimento válida";
            }
            this.updateFormState("dob", value.dob, error);
        }

        if (value.cpf !== registerUserForm.cpf.value) {
            let error = null;
            if (value.cpf.length === 0) {
                error = "Digite um CPF válido";
            }
            this.updateFormState("cpf", value.cpf, error);
        }
    };

    login = async () => {
        let { registerUserForm } = this.state;
        if (
            !registerUserForm.email.pristine &&
            !registerUserForm.email.hasError &&
            !registerUserForm.dob.pristine &&
            !registerUserForm.dob.hasError &&
            !registerUserForm.name.pristine &&
            !registerUserForm.name.hasError &&
            !registerUserForm.cpf.pristine &&
            !registerUserForm.cpf.hasError
        ) {
            try {
                let userForm = {
                    email: registerUserForm.email.value,
                    dob: registerUserForm.dob.value,
                    name: registerUserForm.name.value,
                    cpf: registerUserForm.cpf.value,
                };

                this.props.navigation.navigate('RegisterPassword')

            } catch (error) {
                this.setState({
                    error
                });
            }

        } else {
            Alert.alert("Atenção", "Preencha os campos corretamente");
        }
    };

    render() {
        let { registerUserForm, isFetching } = this.state;

        const options = {
            stylesheet,
            fields: {
                email: {
                    keyboardType: "email-address",
                    editable: !isFetching,
                    hasError: registerUserForm.email.hasError,
                    error: registerUserForm.email.error,
                    value: registerUserForm.email.value,
                    label: 'E-mail',
                    autoCapitalize: "none",
                    autoCorrect: false,
                    clearButtonMode: "always",
                },
                cpf: {
                    keyboardType: "number-pad",
                    editable: !isFetching,
                    hasError: registerUserForm.cpf.hasError,
                    error: registerUserForm.cpf.error,
                    value: registerUserForm.cpf.value,
                    label: 'CPF',
                    autoCapitalize: "none",
                    autoCorrect: false,
                    clearButtonMode: "always",
                },
                dob: {
                    keyboardType: "number-pad",
                    editable: !isFetching,
                    hasError: registerUserForm.dob.hasError,
                    error: registerUserForm.dob.error,
                    value: registerUserForm.dob.value,
                    label: 'Data de Nascimento',
                    autoCapitalize: "none",
                    autoCorrect: false,
                    clearButtonMode: "always",
                },
                name: {
                    editable: !isFetching,
                    hasError: registerUserForm.name.hasError,
                    error: registerUserForm.name.error,
                    value: registerUserForm.name.value,
                    label: 'Nome',
                    autoCapitalize: "none",
                    autoCorrect: false,
                    clearButtonMode: "always",
                },
            }
        };

        const formValue = {
            email: registerUserForm.email.value,
            cpf: registerUserForm.cpf.value,
            name: registerUserForm.name.value,
            dob: registerUserForm.dob.value,
        };

        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.imgContainer}>
                    <Image
                        resizeMode="contain"
                        source={require("../resources/female-photo.png")}
                        style={{ flex: 3 }}
                    />
                </TouchableOpacity>
                <View style={styles.formContainer}>
                    <Form
                        ref="form"
                        type={RegisterUserForm}
                        options={options}
                        value={formValue}
                        onChange={this.onChangeForm}
                    />
                    <ContinueButton onPress={this.login} title={'CONTINUAR'} />
                    {isFetching && (
                        <ActivityIndicator
                            color={"#fff"}
                            size={'large'}
                        />
                    )}
                </View>
                {this.state.error && (
                    <Text style={styles.errorMessage}>{this.state.error.message}</Text>
                )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        padding: 10,
        backgroundColor: '#fff'
    },
    imgContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-end"
    },
    formContainer: {
        flex: 3,
        marginHorizontal: 16,
        alignItems: "stretch",
        flexDirection: "column",
        justifyContent: "flex-start"
    },
    forgotPasswordText: {
        color: Colors.lighterText,
        fontSize: 12,
        alignSelf: "center"
    },
    errorMessage: {
        alignSelf: "center",
        fontWeight: "bold",
        marginBottom: 8,
        color: Colors.errorColor
    },
});

export default RegisterUser;