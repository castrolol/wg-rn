import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Alert
} from "react-native";
import { SocialIcon, Header, Button } from "react-native-elements";
import { StackNavigator } from "react-navigation";
import styled from 'styled-components/native'
import t from "tcomb-form-native";
import Api from "@shared/Api";
import Colors from "@shared/Colors";
import { ActivityIndicator } from "react-native";
import stylesheet from '@shared/FormStyle'

const TextStyled = styled.Text`
    color: ${Colors.primaryColor};
    text-align: center;
    font-size: 18px;
`
const TextButton = styled.Text`
    color: #fff;
`

const Form = t.form.Form;
const LoginForm = t.struct({
    password: t.String
});

class RegisterPassword extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: "Faça seu Cadastro",
        headerStyle: {
            backgroundColor: Colors.primaryColor
        },
        headerRight: (
            <TouchableOpacity
                onPress={navigation.state.params && navigation.state.params.onSubmit}
                style={{ paddingRight: 5 }}
            >
                <TextButton>Salvar</TextButton>
            </TouchableOpacity>
        )
    });


    constructor(props) {
        super(props);

        this.state = {
            loginForm: {
                password: {
                    value: "",
                    hasError: false,
                    pristine: true,
                    error: null
                }
            },
            isFetching: false,
            error: null
        };
    }

    updateFormState(field, value, error) {
        let { loginForm } = this.state;
        this.setState({
            loginForm: {
                ...loginForm,
                [field]: {
                    pristine: false,
                    hasError: !!error,
                    value,
                    error
                }
            },
            error: null
        });
    }

    onChangeForm = value => {
        let { loginForm } = this.state;
        if (value.password !== loginForm.password.value) {
            let error = null;
            if (value.password.length === 0) {
                error = "Digite sua senha";
            }

            this.updateFormState("password", value.password, error);
        }
    };

    goBack() {
        this.props.navigation.goBack();
    }

    componentDidMount() {
        this.props.navigation.setParams({
            onSubmit: this._onSubmit.bind(this)
        });
    }



    _onSubmit() {
        let { loginForm } = this.state;
        if (
            !loginForm.password.pristine &&
            !loginForm.password.hasError
        ) {
            this.setState({ isFetching: true });

            try {
                let userForm = {
                    password: loginForm.password.value,
                };

                // let user = await Api.login(userForm);

                this.props.screenProps.onUserUpdate &&
                    this.props.screenProps.onUserUpdate({ name: 'nikollas' });

                this.goBack();
            } catch (error) {
                this.setState({
                    error
                });
            }

            this.setState({ isFetching: false });
        } else {
            Alert.alert("Atenção", "Preencha a senha");
        }
    };

    render() {
        let { loginForm, isFetching } = this.state;

        const options = {
            stylesheet,
            fields: {
                password: {
                    secureTextEntry: true,
                    editable: !isFetching,
                    label: 'Senha',
                    hasError: loginForm.password.hasError,
                    error: loginForm.password.error,
                    value: loginForm.password.value
                }
            }
        };

        const formValue = {
            password: loginForm.password.value
        };

        return (
            <View style={styles.container}>
                <View style={styles.imgContainer}>
                    <TextStyled>Escolha agora uma senha para usar o aplicativo</TextStyled>
                </View>
                <View style={styles.formContainer}>
                    <Form
                        ref="form"
                        type={LoginForm}
                        options={options}
                        value={formValue}
                        onChange={this.onChangeForm}
                    />
                </View>
                {this.state.error && (
                    <Text style={styles.errorMessage}>{this.state.error.message}</Text>
                )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: '#fff'
    },
    imgContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    formContainer: {
        flex: 3,
        marginHorizontal: 16,
        alignItems: "stretch",
        flexDirection: "column",
        justifyContent: "flex-start"
    },
    forgotPasswordText: {
        color: Colors.lighterText,
        fontSize: 12,
        alignSelf: "center"
    },
    errorMessage: {
        alignSelf: "center",
        fontWeight: "bold",
        marginBottom: 8,
        color: Colors.errorColor
    },
});

export default RegisterPassword;