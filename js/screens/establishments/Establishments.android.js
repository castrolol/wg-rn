import React from 'react'

import { View, Text, FlatList, StatusBar, TextInput, StyleSheet } from 'react-native'
import Colors from '@shared/Colors'
import { RenderDrawerMenu } from "@components/DrawerUtils";
import { Icon } from 'react-native-elements'
import styled from 'styled-components/native'

const Container = styled.View`
    flex: 1;
    backgroundColor: ${Colors.whiteColor}
`
const ContainerItem = styled.View`
    flex-direction: row;
`
const data = [
    { id: 1, name: 'Nome do Estabelecimento', category: 'Categoria  - 60 a 90 min', open: 'Aberto das 11:00 às 15:00' },
    { id: 2, name: 'Nome do Estabelecimento', category: 'Categoria  - 60 a 90 min', open: 'Aberto das 11:00 às 15:00' },
    { id: 3, name: 'Nome do Estabelecimento', category: 'Categoria  - 60 a 90 min', open: 'Aberto das 11:00 às 15:00' },
    { id: 4, name: 'Nome do Estabelecimento', category: 'Categoria  - 60 a 90 min', open: 'Aberto das 11:00 às 15:00' },
    { id: 5, name: 'Nome do Estabelecimento', category: 'Categoria  - 60 a 90 min', open: 'Aberto das 11:00 às 15:00' },
    { id: 6, name: 'Nome do Estabelecimento', category: 'Categoria  - 60 a 90 min', open: 'Aberto das 11:00 às 15:00' },
    { id: 7, name: 'Nome do Estabelecimento', category: 'Categoria  - 60 a 90 min', open: 'Aberto das 11:00 às 15:00' },
]

export default class Establishments extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: "Estabelecimentos",
        drawerLabel: "Estabelecimentos",
        drawerIcon: ({ tintColor }) => (
            <Icon name="trending-up" color={tintColor} />
        ),
        headerRight: RenderDrawerMenu(navigation),
        headerStyle: {
            top: 0,
            left: 0,
            borderBottomWidth: 1,
            right: 0,
            elevation: 0,
        }
    });

    componentDidMount = () => {
        StatusBar.setBarStyle('dark-content')
    };

    renderItem = ({ item }) => {
        return (
            <ContainerItem>
                <Text>{item.name}</Text>
            </ContainerItem>
        )
    }
    render() {
        return (
            <Container>
                <View style={styles.searchSection}>
                    <Icon style={styles.searchIcon} name="search" size={20} backgroundColor={'#f5f5f5'} color={`${Colors.primaryColor}`} />
                    <TextInput
                        style={styles.input}
                        placeholder="Buscar"
                        onChangeText={(searchString) => { this.setState({ searchString }) }}
                        underlineColorAndroid="transparent"
                    />
                </View>
                <FlatList
                    data={data}
                    renderItem={item => this.renderItem(item)}
                />
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    searchSection: {
        flexDirection: 'row',
        padding: 10,
        borderBottomWidth: 1,
        borderColor: '#ccc',
    },
    searchIcon: {
        padding: 3,
        backgroundColor: '#f5f5f5'
    },
    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 5,
        backgroundColor: '#f5f5f5',
        color: '#424242',
    },
})