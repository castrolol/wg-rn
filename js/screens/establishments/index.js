import React, { Component } from "react";
import { StackNavigator } from "react-navigation";

import Establishments from "./Establishments";

import Colors from "@shared/Colors";

export default StackNavigator(
    {
        Establishments: { screen: Establishments }
    },
    {
        initialRouteName: "Establishments",
        navigationOptions: {
            headerBackTitle: null,
            headerTintColor: Colors.lighterText,
            headerStyle: {
                backgroundColor: Colors.primaryColor
            }
        }
    }
);