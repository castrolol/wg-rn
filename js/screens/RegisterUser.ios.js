import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Alert,
} from "react-native";
import { SocialIcon, Header, Button } from "react-native-elements";
import { StackNavigator } from "react-navigation";
import styled from 'styled-components/native'
import t from "tcomb-form-native";
import Api from "@shared/Api";
import Colors from "@shared/Colors";
import { ActivityIndicator } from "react-native";
import stylesheet from '@shared/FormStyle'

const Register = styled.Text`
    color: ${Colors.primaryColor};
    text-align: center;
    margin-top: 30px;
`
const TextButton = styled.Text`
    color: #fff;
`

const Form = t.form.Form;

const RegisterUserForm = t.struct({
    name: t.String,
    email: t.String,
    cpf: t.String,
    dob: t.String
});

class RegisterUser extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: "Faça seu Cadastro",
        headerStyle: {
            backgroundColor: Colors.primaryColor
        },
        headerRight: (
            <TouchableOpacity
                onPress={navigation.state.params && navigation.state.params.onSubmit}
                style={{ paddingRight: 5 }}
            >
                <TextButton>Continuar</TextButton>
            </TouchableOpacity>
        )
    });

    constructor(props) {
        super(props);

        this.state = {
            loginForm: {
                email: {
                    value: "",
                    hasError: false,
                    pristine: true,
                    error: null
                },
                dob: {
                    value: "",
                    hasError: false,
                    pristine: true,
                    error: null
                },
                name: {
                    value: "",
                    hasError: false,
                    pristine: true,
                    error: null
                },
                cpf: {
                    value: "",
                    hasError: false,
                    pristine: true,
                    error: null
                },
            },
            isFetching: false,
            error: null
        };
    }

    componentDidMount() {
        this.props.navigation.setParams({
            onSubmit: this._onSubmit.bind(this)
        });
    }


    updateFormState(field, value, error) {
        let { loginForm } = this.state;
        this.setState({
            loginForm: {
                ...loginForm,
                [field]: {
                    pristine: false,
                    hasError: !!error,
                    value,
                    error
                }
            },
            error: null
        });
    }

    onChangeForm = value => {
        let { loginForm } = this.state;

        if (value.email !== loginForm.email.value) {
            let error = null;
            if (value.email.length === 0) {
                error = "Digite um email válido";
            }
            this.updateFormState("email", value.email, error);
        }

        if (value.name !== loginForm.name.value) {
            let error = null;
            if (value.name.length === 0) {
                error = "Digite um nome";
            }
            this.updateFormState("name", value.name, error);
        }

        if (value.dob !== loginForm.dob.value) {
            let error = null;
            if (value.dob.length === 0) {
                error = "Digite uma Data de Nascimento válida";
            }
            this.updateFormState("dob", value.dob, error);
        }

        if (value.cpf !== loginForm.cpf.value) {
            let error = null;
            if (value.cpf.length === 0) {
                error = "Digite um CPF válido";
            }
            this.updateFormState("cpf", value.cpf, error);
        }
    };

    _onSubmit() {
        let { loginForm } = this.state;
        if (
            !loginForm.email.pristine &&
            !loginForm.email.hasError &&
            !loginForm.dob.pristine &&
            !loginForm.dob.hasError &&
            !loginForm.name.pristine &&
            !loginForm.name.hasError &&
            !loginForm.cpf.pristine &&
            !loginForm.cpf.hasError
        ) {
            try {
                let userForm = {
                    email: loginForm.email.value,
                    dob: loginForm.dob.value,
                    name: loginForm.name.value,
                    cpf: loginForm.cpf.value,
                };
                console.log(userForm)
                this.props.navigation.navigate('RegisterPassword')

            } catch (error) {
                this.setState({
                    error
                });
            }

        } else {
            Alert.alert("Atenção", "Preencha os campos corretamente");
        }
    };

    render() {
        let { loginForm, isFetching } = this.state;

        const options = {
            stylesheet,
            fields: {
                email: {
                    keyboardType: "email-address",
                    editable: !isFetching,
                    hasError: loginForm.email.hasError,
                    error: loginForm.email.error,
                    value: loginForm.email.value,
                    label: 'E-mail',
                    autoCapitalize: "none",
                    autoCorrect: false,
                    clearButtonMode: "always",
                },
                cpf: {
                    keyboardType: "number-pad",
                    editable: !isFetching,
                    hasError: loginForm.cpf.hasError,
                    error: loginForm.cpf.error,
                    value: loginForm.cpf.value,
                    label: 'CPF',
                    autoCapitalize: "none",
                    autoCorrect: false,
                    clearButtonMode: "always",
                },
                dob: {
                    keyboardType: "number-pad",
                    editable: !isFetching,
                    hasError: loginForm.dob.hasError,
                    error: loginForm.dob.error,
                    value: loginForm.dob.value,
                    label: 'Data de Nascimento',
                    autoCapitalize: "none",
                    autoCorrect: false,
                    clearButtonMode: "always",
                },
                name: {
                    editable: !isFetching,
                    hasError: loginForm.name.hasError,
                    error: loginForm.name.error,
                    value: loginForm.name.value,
                    label: 'Nome',
                    autoCapitalize: "none",
                    autoCorrect: false,
                    clearButtonMode: "always",
                },
            }
        };

        const formValue = {
            email: loginForm.email.value,
            cpf: loginForm.cpf.value,
            name: loginForm.name.value,
            dob: loginForm.dob.value,
        };

        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.imgContainer}>
                    <Image
                        resizeMode="contain"
                        source={require("../resources/female-photo.png")}
                        style={{ flex: 3 }}
                    />
                </TouchableOpacity>
                <View style={styles.formContainer}>
                    <Form
                        ref="form"
                        type={RegisterUserForm}
                        options={options}
                        value={formValue}
                        onChange={this.onChangeForm}
                    />
                </View>
                {this.state.error && (
                    <Text style={styles.errorMessage}>{this.state.error.message}</Text>
                )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: '#fff'
    },
    imgContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-end"
    },
    formContainer: {
        flex: 3,
        marginHorizontal: 16,
        alignItems: "stretch",
        flexDirection: "column",
        justifyContent: "flex-start"
    },
    forgotPasswordText: {
        color: Colors.lighterText,
        fontSize: 12,
        alignSelf: "center"
    },
    errorMessage: {
        alignSelf: "center",
        fontWeight: "bold",
        marginBottom: 8,
        color: Colors.errorColor
    },
});

export default RegisterUser;