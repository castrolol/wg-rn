import cloneDeep from "lodash/fp/cloneDeep";
import t from "tcomb-form-native";
import Colors from '@shared/Colors'

const stylesheet = cloneDeep(t.form.Form.stylesheet);

stylesheet.textbox.normal.color = Colors.primaryColor;
stylesheet.textbox.normal.borderWidth = 0;
stylesheet.textbox.normal.borderBottomWidth = 1;
stylesheet.textbox.normal.borderColor = Colors.primaryColor;
stylesheet.controlLabel.normal.color = Colors.primaryColor;
stylesheet.controlLabel.normal.fontSize = 13;

//error style
stylesheet.textbox.error.color = Colors.primaryColor;
stylesheet.textbox.error.borderWidth = 0;
stylesheet.textbox.error.borderBottomWidth = 1;
stylesheet.textbox.error.borderColor = Colors.primaryColor;
stylesheet.textbox.error.color = Colors.primaryColor;
stylesheet.controlLabel.error.color = Colors.primaryColor;

export default stylesheet