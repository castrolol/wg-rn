import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import styled from 'styled-components/native'
import Colors from '@shared/Colors'

const Button = styled.TouchableOpacity`
    border-radius: 4px;
    margin-top: 10px;
    background-color: ${Colors.primaryColor};
    padding-top: 8px;
    padding-bottom: 8px;
    padding-left: 15px;
    padding-right: 15px;
`
const TextButton = styled.Text`
    color: #fff;
`
const ContainerButton = styled.View`
    flex-direction: row;
    justify-content: flex-end;
`

const ContinueButton = props => (
    <ContainerButton>
        <Button onPress={props.onPress}>
            <TextButton>{props.title}</TextButton>
        </Button>
    </ContainerButton>
)

export default ContinueButton